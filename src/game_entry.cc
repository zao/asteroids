#include "asset_load_service.h"
#include "game_host.h"
#include "game_interface.h"
#include "service_guids.h"

#include <iostream>
#include <list>

#ifdef ZUL_OS_WINDOWS
#include <Windows.h>
#endif
#include <GL/glew.h>

static GLenum
internal_format_for_components(int components)
{
  if (components == 1)
    return GL_R8;
  if (components == 2)
    return GL_RG8;
  if (components == 3)
    return GL_RGB8;
  if (components == 4)
    return GL_RGBA8;
  return 0;
}

static GLenum
format_for_components(int components)
{
  if (components == 1)
    return GL_RED;
  if (components == 2)
    return GL_RG;
  if (components == 3)
    return GL_RGB;
  if (components == 4)
    return GL_RGBA;
  return 0;
}

struct asteroids_game : game_interface
{
  asset_load_service* asset_load = nullptr;
  struct pending_texture_load
  {
    pending_texture_load(std::future<image_data> pixel_data)
      : pixel_data(std::move(pixel_data))
    {
    }

    std::future<image_data> pixel_data;
  };
  std::list<pending_texture_load> active_image_loads;

  bool initialize(game_host* host) override
  {
    std::cerr << "Hi!\n";
    now = 0.0;
    host_service_group* bleh =
      host->find_service_group(service_guids::asset_load_service_class_id);
    if (bleh && bleh->instance_slist) {
      this->asset_load = (asset_load_service*)bleh->instance_slist;
      active_image_loads.emplace_back(
        asset_load->load_image("data/tileBlue_25.png", 4, 1));
      active_image_loads.emplace_back(
        asset_load->load_image("data/enoent.png", 4, 1));
      asset_load->load_datafile("data/enoent.dat").get();
    }
    return true;
  }

  void resize(int width, int height) override
  {
    std::cerr << "Game says new size is " << width << "x" << height << "\n";
  }

  double now;
  double const dt = 1.0 / 144.0;

  void update() override
  {
    for (auto I = active_image_loads.begin(); I != active_image_loads.end();) {
      if (I->pixel_data.wait_for(std::chrono::seconds(0)) ==
          std::future_status::ready) {
        auto res = I->pixel_data.get();
        I = active_image_loads.erase(I);
        if (res.mips) {
          auto& top = res.image_levels[0];
          std::cerr << "Image: " << top.width << "x" << top.height << ", "
                    << res.components << " components.\n";
          GLuint tex = 0;
          glCreateTextures(GL_TEXTURE_2D, 1, &tex);
          GLenum internal_format =
            internal_format_for_components(res.components);
          GLenum format = format_for_components(res.components);
          glTextureStorage2D(tex, res.mips, internal_format, top.width,
                             top.height);
          for (int mip = 0; mip < res.mips; ++mip) {
            auto& lvl = res.image_levels[mip];
            glTextureSubImage2D(tex, mip, 0, 0, lvl.width, lvl.height, format,
                                GL_UNSIGNED_BYTE, lvl.pixels.get());
          }
          glDeleteTextures(1, &tex);
        }
      } else {
        ++I;
      }
    }
    now += dt;
    float r = 0.5f + 0.5f * sinf((float)now * 30.0f);
    float g = 0.5f + 0.5f * sinf((float)now + 0.3f);
    float b = 0.5f + 0.5f * sinf((float)now + 0.6f);
    glClearColor(0.1f * r, 0.1f * g, 0.1f * b, 1.0f);
  }

  void terminate() override { std::cerr << "Bye!\n"; }
};

static asteroids_game game;

extern "C" {
ZUL_EXPORT
game_interface*
get_game_interface()
{
  return &game;
}
}
