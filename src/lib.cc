#include "lib.h"

#ifdef ZUL_OS_WINDOWS
#include <Windows.h>

void
game_library::close()
{
  if (this->handle) {
    FreeLibrary((HMODULE)this->handle);
  }
  this->handle = nullptr;
  this->entrypoint = nullptr;
}

game_library
try_load_library(std::string name)
{
  HMODULE h = LoadLibraryA(name.c_str());
  if (!h) {
    return {};
  }
  FARPROC entrypoint = GetProcAddress(h, "get_game_interface");
  if (!entrypoint) {
    FreeLibrary(h);
    return {};
  }
  game_library ret;
  ret.handle = h;
  ret.entrypoint = entrypoint;
  return ret;
}

#else

void
game_library::close()
{
  if (this->handle) {
    dlclose(this->handle);
  }
  this->handle = nullptr;
  this->entrypoint = nullptr;
}

game_library
try_load_library(std::string name)
{
  void* game_lib = dlopen(name.c_str(), RTLD_NOW);
  if (!game_lib) {
    return {};
  }
  void* entrypoint = dlsym(game_lib, "get_game_interface");
  if (!entrypoint) {
    dlclose(game_lib);
    return {};
  }
  game_library ret;
  ret.handle = game_lib;
  ret.entrypoint = entrypoint;
  return ret;
}
#endif
