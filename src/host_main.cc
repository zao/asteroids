#include "asset_load_service.h"
#include "game_host.h"
#include "game_interface.h"
#include "lib.h"
#include "service_guids.h"

#include <fstream>
#include <future>
#include <iostream>
#include <map>
#include <memory>
#include <string.h>
#include <string>
#include <time.h>

#ifdef ZUL_OS_WINDOWS
#include <Windows.h>
#else
#include <dirent.h>
#include <dlfcn.h>
#include <ftw.h>
#endif

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "stb_image.h"

struct sys_data
{
  GLFWwindow* wnd;
};

static sys_data sys;

static int window_hints[] = {
  GLFW_RESIZABLE,
  GL_TRUE,
  GLFW_VISIBLE,
  GL_TRUE,
  GLFW_MAXIMIZED,
  GL_TRUE,
  GLFW_CONTEXT_VERSION_MAJOR,
  4,
  GLFW_CONTEXT_VERSION_MINOR,
  5,
  GLFW_OPENGL_PROFILE,
  GLFW_OPENGL_CORE_PROFILE,
  GLFW_OPENGL_DEBUG_CONTEXT,
  GL_TRUE,
  GLFW_SAMPLES,
  16,
};
static int num_window_hints = sizeof(window_hints) / sizeof(window_hints[0]);

host_service* first_service;

void
sys_init()
{
  glfwInit();
  sys.wnd = glfwCreateWindow(1280, 720, "", nullptr, nullptr);
  glfwMakeContextCurrent(sys.wnd);
  glewInit();
}

void
sys_terminate()
{
  glfwDestroyWindow(sys.wnd);
  glfwTerminate();
}

#ifdef ZUL_OS_WINDOWS
using file_time = FILETIME;

bool
operator<(FILETIME a, FILETIME b)
{
  if (a.dwHighDateTime != b.dwHighDateTime)
    return a.dwHighDateTime < b.dwHighDateTime;
  return a.dwLowDateTime < b.dwLowDateTime;
}

bool
operator>(FILETIME a, FILETIME b)
{
  if (a.dwHighDateTime != b.dwHighDateTime)
    return a.dwHighDateTime > b.dwHighDateTime;
  return a.dwLowDateTime > b.dwLowDateTime;
}
#else
using file_time = struct timespec;

bool
operator<(struct timespec a, struct timespec b)
{
  if (a.tv_sec != b.tv_sec)
    return a.tv_sec < b.tv_sec;
  return a.tv_nsec < b.tv_nsec;
}

bool
operator>(struct timespec a, struct timespec b)
{
  if (a.tv_sec != b.tv_sec)
    return a.tv_sec > b.tv_sec;
  return a.tv_nsec > b.tv_nsec;
}
#endif

struct game_find_result
{
  std::string name;
  file_time time;
};

static std::string newest_game_name;
static file_time newest_game_time;

static game_find_result
find_newest_game(char const* dir)
{
  newest_game_name = "";
  newest_game_time = {};
#ifdef ZUL_OS_WINDOWS
  std::string pattern = dir + std::string("\\*.ready");
  WIN32_FIND_DATAA find_data{};
  HANDLE h = FindFirstFileA(pattern.c_str(), &find_data);
  if (h) {
    do {
      char const* fpath = find_data.cFileName;
      FILETIME candidate_time = find_data.ftLastWriteTime;
      if (candidate_time > newest_game_time) {
        char const* rdot = strrchr(fpath, '.');
        if (rdot && strcmp(rdot, ".ready") == 0) {
          newest_game_time = candidate_time;
          newest_game_name = std::string(fpath, rdot - fpath);
        }
      }
    } while (FindNextFileA(h, &find_data));
  }
  FindClose(h);
#else
  ftw(dir,
    [](char const* fpath, struct stat const* sb, int typeflag) -> int {
    struct timespec candidate_time = sb->st_mtim;
    if (candidate_time > newest_game_time && typeflag == FTW_F) {
      char const* rdot = strrchr(fpath, '.');
      if (rdot && strcmp(rdot, ".ready") == 0) {
        newest_game_time = candidate_time;
        newest_game_name = std::string(fpath, rdot - fpath);
      }
    }
    return 0;
  },
    10);
#endif
  if (newest_game_name.empty()) {
    return{};
  }
  return{ newest_game_name, newest_game_time };
}

host_service_group* service_groups;

void
register_service(guid class_id, host_service* instance)
{
  for (host_service_group* cur = service_groups; cur; cur = cur->next_service) {
    if (cur->class_id == class_id) {
      instance->next_instance = cur->instance_slist;
      cur->instance_slist = instance;
      return;
    }
  }

  host_service_group* g = new host_service_group;
  g->class_id = class_id;
  g->instance_slist = instance;
  g->next_service = service_groups;
  service_groups = g->next_service;
}

struct host_impl : game_host
{
  virtual host_service_group* find_service_group(guid class_id);
} host;

host_service_group*
host_impl::find_service_group(guid class_id)
{
  for (host_service_group* cur = service_groups; cur; cur = cur->next_service) {
    if (cur->class_id == class_id) {
      return cur;
    }
  }
  return nullptr;
}

game_library current_game_lib;
game_interface* current_game;
std::string current_game_name;
file_time current_game_time;

using game_entry_sig = game_interface* (*)();

int
real_main()
{
  sys_init();
  int knownWindowWidth, knownWindowHeight;
  glfwGetWindowSize(sys.wnd, &knownWindowWidth, &knownWindowHeight);

  bool queuedPresent = true;
  double lastTickTime = 0.0;
  double dt = 1.0 / 144.0;
  while (1) {
    auto game_result = find_newest_game("bin");
    if (game_result.name != current_game_name) {
      std::cerr << "Loading library \"" << game_result.name << "\"\n";
      game_library lib = try_load_library(game_result.name);
      if (lib.entrypoint) {
        game_entry_sig entry = (game_entry_sig)lib.entrypoint;
        auto* new_game = entry();
        if (current_game) {
          current_game->terminate();
          current_game = nullptr;
        }
        if (current_game_lib) {
          current_game_lib.close();
        }
        current_game = new_game;
        current_game->initialize(&host);
        current_game_name = newest_game_name;
        current_game_time = newest_game_time;
        current_game_lib = lib;
        current_game->resize(knownWindowWidth, knownWindowHeight);
      }
    }

    {
      int w, h;
      glfwGetWindowSize(sys.wnd, &w, &h);
      if (w != knownWindowWidth || h != knownWindowHeight) {
        knownWindowWidth = w;
        knownWindowHeight = h;
        if (current_game) {
          current_game->resize(w, h);
        }
      }
    }

    // fetch input
    glfwPollEvents();
    if (glfwWindowShouldClose(sys.wnd)) {
      break;
    }
    // update + draw
    if (current_game) {
      double now = glfwGetTime();
      if (lastTickTime + dt <= now) {
        current_game->update();
        lastTickTime = now;
      }
      if (queuedPresent) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glfwSwapBuffers(sys.wnd);
      }
    }
  }
  if (current_game) {
    current_game->terminate();
  }
  if (current_game_lib) {
    current_game_lib.close();
  }
  sys_terminate();
  return 0;
}

#ifdef ZUL_OS_WINDOWS
int WINAPI
WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
  return real_main();
}
#else
int
main()
{
  return real_main();
}
#endif
