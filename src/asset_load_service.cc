#include "asset_load_service.h"
#include "service_guids.h"

#include "stb_image.h"
#include "stb_image_resize.h"
#include <fstream>

struct asset_load_impl : asset_load_service
{
  std::future<datafile_data> load_datafile(std::string filename) override;
  std::future<image_data> load_image(std::string filename, int component_count,
                                     int mips) override;
};

std::future<datafile_data>
asset_load_impl::load_datafile(std::string filename)
{
  std::future<datafile_data> ret = std::async(std::launch::async, [filename] {
    datafile_data data;
    {
      std::ifstream is(filename);
      if (is) {
        is.seekg(0, std::ios::end);
        int size = (int)is.tellg();
        is.seekg(0, std::ios::beg);
        if (size) {
          data.data = std::make_unique<char[]>(size);
          data.size = size;
          is.read(data.data.get(), size);
        }
      }
    }
    return data;
  });
  return ret;
}

static int
nth_mip(int f, int n)
{
  f /= (1 << n);
  return f ? f : 1;
}

static int
count_mips(int w, int h)
{
  int x = (w > h) ? w : h;
  int mips = 1;
  while (x != 1) {
    x /= 2;
    ++mips;
  }
  return mips;
}

std::future<image_data>
asset_load_impl::load_image(std::string filename, int component_count, int mips)
{
  std::future<image_data> ret =
    std::async(std::launch::async, [filename, component_count, mips] {
      image_data data;
      {
        int w, h, comp;
        auto* pixels =
          stbi_load(filename.c_str(), &w, &h, &comp, component_count);
        if (pixels) {
          int storage_needed = w * h * component_count;
          if (mips > 0) {
            data.mips = mips;
          } else {
            data.mips = count_mips(w, h);
          }
          data.image_levels =
            std::make_unique<image_data::image_level[]>(data.mips);
          for (int mip = 0; mip < data.mips; ++mip) {
            image_data::image_level lvl = {};
            lvl.width = nth_mip(w, mip);
            lvl.height = nth_mip(h, mip);
            lvl.pixels = std::make_unique<char[]>(lvl.width * lvl.height *
                                                  component_count);
            if (mip == 0) {
              memcpy(lvl.pixels.get(), pixels,
                     lvl.width * lvl.height * component_count);
            } else {
              stbir_resize_uint8(
                (unsigned char*)pixels, w, h, w * component_count,
                (unsigned char*)lvl.pixels.get(), lvl.width, lvl.height,
                lvl.width * component_count, component_count);
            }
            data.image_levels[mip] = std::move(lvl);
          }
          data.components = component_count;
          data.source_components = comp;
          data.mips = mips;
        }
        stbi_image_free(pixels);
      }
      return data;
    });
  return ret;
}

static service_factory<asset_load_impl> g_loader(
  service_guids::asset_load_service_class_id);
