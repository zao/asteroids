CC="clang"
CXX="clang++"
CPPFLAGS="-I include"
CXXFLAGS="-std=c++1y -fvisibility=hidden -g"
LDFLAGS="-fPIC -ldl -pthreads"

${CXX} ${CPPFLAGS} ${CXXFLAGS} ${LDFLAGS} \
    -o bin/host src/host_main.cc
LIBBASE=$(mktemp -p bin libgame_XXXXXXXXXX.so)
${CXX} ${CPPFLAGS} ${CXXFLAGS} ${LDFLAGS} \
    -DZUL_BUILDING_SHLIB -rdynamic -shared -o ${LIBBASE} src/game_entry.cc && touch ${LIBBASE}.ready || touch ${LIBBASE}.failed
