@echo off && setlocal EnableDelayedExpansion
call "%VS140COMNTOOLS%\VsDevCmd.bat"
call "%VCINSTALLDIR%\vcvarsall.bat" x64 
set DEPROOT=X:\libs\vc14-x64-release
set CXX="C:\Program Files\LLVM\bin\clang++.exe"
set LIBID=!RANDOM!!RANDOM!!RANDOM!
set LIBNAME=bin\game%LIBID%.dll
set CPPFLAGS=-Iinclude -I%DEPROOT%\include
set CXXFLAGS=-g -std=c++1y
set LDFLAGS=-L %DEPROOT%\lib
set CommonCompilerFlags=-O2 -MD -nologo -fp:fast -fp:except- -Gm- -GR -EHsc -Zo -Oi -W3 -wd4201 -wd4189 -wd4505 -wd4127 -FC -Z7
set CommonCompilerFlags=-D_CRT_SECURE_NO_DEPRECATE -D_CRT_SECURE_NO_WARNINGS -D_SCL_SECURE_NO_DEPRECATE -D_SCL_SECURE_NO_WARNINGS %CommonCompilerFlags%
set CommonCompilerFlags=-Iinclude -I%DEPROOT%\include %CommonCompilerFlags%
set CommonLinkerFlags=-incremental:no -opt:ref /LIBPATH:%DEPROOT%\lib
cl %CommonCompilerFlags% /Fobin\ /Fe%LIBNAME% -DZUL_BUILDING_SHLIB src\game_entry.cc /LD /link %CommonLinkerFlags% opengl32.lib glfw3dll.lib glew32.lib && copy NUL %LIBNAME%.ready
cl %CommonCompilerFlags% /Fobin\ /Febin\host.exe src\host_main.cc src\lib.cc src\stb.cc src\asset_load_service.cc /link %CommonLinkerFlags% opengl32.lib glfw3dll.lib glew32.lib
copy %DEPROOT%\bin\glew32.dll bin\ 2>NUL
copy %DEPROOT%\bin\glfw3.dll bin\  2>NUL
