#pragma once

#ifdef _WIN32
#define ZUL_OS_WINDOWS
#endif
#ifdef __gnu_linux__
#define ZUL_OS_LINUX
#endif

#ifdef ZUL_OS_WINDOWS
#ifdef ZUL_BUILDING_SHLIB
#define ZUL_EXPORT __declspec(dllexport)
#else
#define ZUL_EXPORT __declspec(dllimport)
#endif
#else
#ifdef ZUL_BUILDING_SHLIB
#define ZUL_EXPORT __attribute__((visibility("default")))
#else
#define ZUL_EXPORT
#endif
#endif