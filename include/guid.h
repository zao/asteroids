#pragma once

#include <stdint.h>
#include <string.h>

struct guid
{
  uint64_t data[2];
};

inline bool
operator<(guid a, guid b)
{
  return memcmp(a.data, b.data, sizeof(guid::data)) < 0;
}

inline bool
operator>(guid a, guid b)
{
  return memcmp(a.data, b.data, sizeof(guid::data)) > 0;
}

inline bool
operator==(guid a, guid b)
{
  return memcmp(a.data, b.data, sizeof(guid::data)) == 0;
}

inline bool
operator!=(guid a, guid b)
{
  return memcmp(a.data, b.data, sizeof(guid::data)) != 0;
}
