#pragma once
#include "service.h"

#include <future>
#include <memory>
#include <string>

struct base_data
{
};

struct datafile_data : base_data
{
  std::unique_ptr<char[]> data;
  int size = 0;
};

struct image_data : base_data
{
  struct image_level
  {
    std::unique_ptr<char[]> pixels;
    int width, height;
  };
  std::unique_ptr<image_level[]> image_levels;
  int components = 0;
  int source_components = 0;
  int mips = 0;
};

struct asset_load_service : host_service
{
  virtual std::future<datafile_data> load_datafile(std::string filename) = 0;
  virtual std::future<image_data> load_image(std::string filename,
                                             int component_count, int mips) = 0;
};
