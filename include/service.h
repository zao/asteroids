#pragma once

#include "guid.h"

struct host_service
{
  host_service* next_instance = 0;
};

struct host_service_group
{
  guid class_id;
  host_service_group* next_service = 0;
  host_service* instance_slist = 0;
};

void register_service(guid, host_service*);

template <typename Impl>
struct service_factory
{
  service_factory(guid class_id) {
    register_service(class_id, &impl);
  }
  Impl impl;
};
