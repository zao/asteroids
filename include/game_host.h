#pragma once
#include "guid.h"
#include "service.h"

struct game_host
{
  virtual host_service_group* find_service_group(guid class_id) = 0;
};
