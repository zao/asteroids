#pragma once
#include "defs.h"
#include <string>

struct game_library
{
  void* handle = nullptr;
  void* entrypoint = nullptr;
  void close();
  operator bool() const { return !!entrypoint; }
};

game_library try_load_library(std::string name);