#pragma once
#include "defs.h"

struct game_host;

struct game_interface
{
  virtual bool initialize(game_host*) = 0;
  virtual void resize(int width, int height) = 0;
  virtual void update() = 0;
  virtual void terminate() = 0;
};
