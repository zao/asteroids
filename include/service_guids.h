#pragma once

#include "guid.h"

namespace {
namespace service_guids {
guid honk_service_class_id = { 0x041F1C677E7584D3u, 0xD58A482F2B1A42BBu };
guid asset_load_service_class_id = { 0xF75DAB60E8F857CAu, 0x667F477A925C3F8Bu };
}
}
